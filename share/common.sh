#!/bin/sh

# Logging functions
LOG_FACILITY=${LOG_FACILITY:-daemon}

error() {
    logger -s -t "linux-client-update" -p $LOG_FACILITY.err "ERROR: $*"
}

fatal() {
    error "$@"
    exit 1
}

info() {
    logger -s -t "linux-client-update" -p $LOG_FACILITY.info "INFO: $*"
}

warning() {
    logger -s -t "linux-client-update" -p $LOG_FACILITY.warning "WARNING: $*"
}

check_rsync_ok() {
    . /etc/linux-client-update.conf || fatal "Missing configuration file."

    if [ -z "$RSYNC_URL" ]; then
        echo "INFO: No rsync URL configured, skipping $0."
        exit 0
    fi

    mkdir -p /var/lib/linux-client-update/rsync
    cd /var/lib/linux-client-update/rsync

    if [ -e RSYNC-FAILED ]; then
        echo "INFO: skipping $0 since last rsync run failed."
        exit 1
    fi
}

# Reboot management functions
plan_reboot() {
    if [ ! -e /var/lib/linux-client-update/reboot-needed ]; then
        mkdir -p /var/lib/linux-client-update
        echo "$*" >>/var/lib/linux-client-update/reboot-needed
        echo 'test -e /var/lib/linux-client-update/reboot-needed && shutdown -r +1' | at 6am tomorrow
    fi
}
cancel_reboot() {
    rm -f /var/lib/linux-client-update/reboot-needed
}

# Send information back to the admin
inform_admin() {
    cat >>/var/lib/linux-client-update/admin-messages
}

send_msg_to_admin() {
    if [ -s /var/lib/linux-client-update/admin-messages ]; then
        mail -s "Upgrade info of $(hostname -f) at $(date +%Y%m%d)" root \
            </var/lib/linux-client-update/admin-messages
    fi
}

