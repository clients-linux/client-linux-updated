# linux Client Update Daemon

## Objectif du paquet

Ce dépôt permet de créer un paquet debian (testé pour l'instant sur debian 10 buster, debian 11 bullseye et ubuntu 18.04 LTS Bionic Beaver) qui permet la mise à jour automatisée des clients linux. Le principe de fonctionnement est le suivant :
 * Sur le serveur linux, un dépôt rsync propose l'ensemble des scripts de mise à jour
 * les client font périodiquement, ainsi qu'au reboot une synchronisation de ces scripts et les exécutent.
 
 Le premier avantage est de ne devoir gérer les scripts que sur le serveur.
 Le second avantage est de pouvoir utiliser des scripts dans n'importe quel langage de programmation (bash/perl/python/php) pour peu que celui ci soit sur le client
 
## Compilation du paquet

Le paquet est construit suivant les standard débian. 
Pour construire le paquet, il suffit de cloner le dépot git puis de lancer la commande

gbp buildpackage

(gbp se trouve dans le paquet git-buildpackage et permet de compiler de façon propre et automatisée un paquet debian issu d'un dépot GIT)

## Préparation du serveur

Le serveur doit disposer d'un dépot rsync qui sera synchronisé systématiquement avec les clients linux.

Un paquet pour cette installation sera prévu dans le futur, actuellement il faut le faire à la main

```bash
apt install rsync
```

On édite ensuite le fichier /etc/rsyncd.conf en y incluant un code de ce genre

```bash
## client linux update
[clientupdate]
    comment = maj clients linux
    path = /srv/linux-client-updates/
    read only = true
    hosts allow = 172.21.200.0/21
    timeout = 600
    auth users=linux
    max connections = 0
    secrets file=/etc/rsyncd.secrets
```

On édite le fichier /etc/rsyncd.secrets et on y met l'utilisateur et le mot de passe qui va avec

```bash
linux:open
```

**Attention**
Le fichier /etc/rsyncd.secrets doit être en mode r-- sinon rsyncd va se plaindre que la sécurité du bouzin est un peu faible :D

on autorise le service rsync au démarrage en éditant le fichier /etc/default/rsync
```
RSYNC_ENABLE=true
```

on lance le service rsync avec 
```bash
systemctl start rsync.service
```

Et voilà, le serveur est configuré, vous n'avez plus qu'à mettre dans /srv/linux-client-updates les scripts de mise à jour

