#
# Regular cron jobs for the linux-client-updated package
#

SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

@reboot     root    [ -x /usr/sbin/linux-client-update ] && sleep 120s && /usr/sbin/linux-client-update
33 3	* * *	root	[ -x /usr/sbin/linux-client-update ] && /usr/sbin/linux-client-update
45 12	* * *	root	[ -x /usr/sbin/linux-client-update ] && /usr/sbin/linux-client-update
