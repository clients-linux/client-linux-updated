#!/bin/sh

. /usr/share/linux-client-update/common.sh || exit 128
. /etc/linux-client-update.conf || fatal "Missing config file"

$ENABLE_APT_UPGRADE || {
    echo "INFO: Apt upgrade disabled in /etc/linux-client-update, exiting."
    exit 0
}

# Ensure no other package manager is running
killall apt-get 2>/dev/null && sleep 5
killall aptitude 2>/dev/null && sleep 5
killall dpkg-preconfigure 2>/dev/null && sleep 5

# Generic options for APT to make it non-interactive
APT_OPTS='-o Dpkg::Options::=--force-confdef -o Dpkg::Options::=--force-confold --yes'
export DEBIAN_FRONTEND="noninteractive"

# Update the lists (try twice)
echo "INFO: Running apt-get update"
apt-get -qq update || (sleep 10; apt-get -qq update)
if [ $? -ne 0 ]; then
    error "apt-get update failed (see $linux_CLIENT_UPDATE_LOGFILE for details)."
    exit 1
fi

if [ -n "$(apt-get -qq -s -y -f install)" ]; then
    warning "Some packages are not correctly installed. Trying to recover with apt-get -f install."
    apt-get $APT_OPTS -f install
    if [ $? -ne 0 ]; then
        error "Recovery failed (see $linux_CLIENT_UPDATE_LOGFILE for details)."
    fi
    auditlog=$(dpkg -C 2>&1)
    aptlog=$(apt-get -qq -s -y -f install)
    if [ -n "$auditlog" ] || [ -n "$aptlog" ]; then
        inform_admin <<END
Some packages are not correctly installed on $(hostname -f).
Here's the "dpkg -C" output:
$auditlog
Here's the "apt-get -s -f install" output:
$aptlog

Check $linux_CLIENT_UPDATE_LOGFILE on the host for more
details about the failure.

END
        exit 2
    fi
fi

# Run the upgrade
echo "INFO: Running apt-get dist-upgrade"

# Get the list of dist-upgrade candidates
APT_RESULT=$(apt-get -dsy dist-upgrade 2>&1)
CANDIDATES=$(echo -n "$APT_RESULT" | grep Inst | awk '{print $2}' | tr '\n' ' ')

if [ "$CANDIDATES" = "" ]; then
    info "APT: no package to upgrade (system is up-to-date)."
    exit 0
fi

info "APT: Upgrading packages: $CANDIDATES"
apt-get $APT_OPTS -qq dist-upgrade
if [ $? -ne 0 ]; then
    error "apt-get failed during dist-upgrade (see $linux_CLIENT_UPDATE_LOGFILE for details)."
    inform_admin <<END
apt-get dist-upgrade failed on $(hostname -f).
It was trying to install those packages:
$CANDIDATES

Check $linux_CLIENT_UPDATE_LOGFILE on the host for more
details about the failure.

END
    exit 3
fi
 
if echo "$CANDIDATES" | grep -q linux-image; then
    plan_reboot "New kernel installed, reboot needed."
fi

# Remove installed packages
apt-get clean

